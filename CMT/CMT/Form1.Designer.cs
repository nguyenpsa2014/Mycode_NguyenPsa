﻿namespace CMT
{
    partial class frmDangKy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.bindingSourceTinhThanh = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceQuanHuyen = new System.Windows.Forms.BindingSource(this.components);
            this.cbNam = new System.Windows.Forms.CheckBox();
            this.cbNu = new System.Windows.Forms.CheckBox();
            this.dtpBirthDay = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lueDkHuyen = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueDkTinh = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueQueHuyen = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTinhThanh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaQuanHuyen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenQuanHuyen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueQueTinh = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMaTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenTinh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVietTat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnLuu = new System.Windows.Forms.Button();
            this.pbxAnh = new DevExpress.XtraEditors.PictureEdit();
            this.dgThongTin = new System.Windows.Forms.DataGridView();
            this.colHoTen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGioiTinh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNgaySinh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDkHKTT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAnh = new System.Windows.Forms.DataGridViewImageColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceTinhThanh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceQuanHuyen)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueDkHuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDkTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueQueHuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueQueTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxAnh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgThongTin)).BeginInit();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label7.Location = new System.Drawing.Point(53, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 19);
            this.label7.TabIndex = 10;
            this.label7.Text = "Ngày Sinh";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label1.Location = new System.Drawing.Point(411, 130);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 19);
            this.label1.TabIndex = 11;
            this.label1.Text = "Quận/Huyện";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label2.Location = new System.Drawing.Point(184, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 19);
            this.label2.TabIndex = 12;
            this.label2.Text = "Tỉnh/TP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label3.Location = new System.Drawing.Point(53, 184);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 19);
            this.label3.TabIndex = 13;
            this.label3.Text = "Nơi ĐK HKTT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label4.Location = new System.Drawing.Point(53, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 19);
            this.label4.TabIndex = 14;
            this.label4.Text = "Quê Quán";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label5.Location = new System.Drawing.Point(53, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 19);
            this.label5.TabIndex = 15;
            this.label5.Text = "Họ Tên";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label6.Location = new System.Drawing.Point(184, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 19);
            this.label6.TabIndex = 16;
            this.label6.Text = "Tỉnh/TP";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label8.Location = new System.Drawing.Point(411, 184);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 19);
            this.label8.TabIndex = 17;
            this.label8.Text = "Quận/Huyện";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label9.Location = new System.Drawing.Point(435, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 19);
            this.label9.TabIndex = 18;
            this.label9.Text = "Giới Tính";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(188, 35);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(202, 21);
            this.txtName.TabIndex = 19;
            // 
            // bindingSourceTinhThanh
            // 
            this.bindingSourceTinhThanh.DataSource = typeof(CMT.TinhThanh);
            // 
            // bindingSourceQuanHuyen
            // 
            this.bindingSourceQuanHuyen.DataSource = typeof(CMT.QuanHuyen);
            // 
            // cbNam
            // 
            this.cbNam.AutoSize = true;
            this.cbNam.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbNam.Location = new System.Drawing.Point(533, 33);
            this.cbNam.Name = "cbNam";
            this.cbNam.Size = new System.Drawing.Size(55, 21);
            this.cbNam.TabIndex = 25;
            this.cbNam.Text = "Nam";
            this.cbNam.UseVisualStyleBackColor = true;
            // 
            // cbNu
            // 
            this.cbNu.AutoSize = true;
            this.cbNu.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbNu.Location = new System.Drawing.Point(594, 33);
            this.cbNu.Name = "cbNu";
            this.cbNu.Size = new System.Drawing.Size(45, 21);
            this.cbNu.TabIndex = 26;
            this.cbNu.Text = "Nữ";
            this.cbNu.UseVisualStyleBackColor = true;
            // 
            // dtpBirthDay
            // 
            this.dtpBirthDay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBirthDay.Location = new System.Drawing.Point(188, 80);
            this.dtpBirthDay.Name = "dtpBirthDay";
            this.dtpBirthDay.Size = new System.Drawing.Size(200, 21);
            this.dtpBirthDay.TabIndex = 29;
            // 
            // panel1
            // 
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.lueDkHuyen);
            this.panel1.Controls.Add(this.lueDkTinh);
            this.panel1.Controls.Add(this.lueQueHuyen);
            this.panel1.Controls.Add(this.lueQueTinh);
            this.panel1.Controls.Add(this.btnLuu);
            this.panel1.Controls.Add(this.pbxAnh);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(954, 280);
            this.panel1.TabIndex = 32;
            // 
            // lueDkHuyen
            // 
            this.lueDkHuyen.Location = new System.Drawing.Point(533, 186);
            this.lueDkHuyen.Name = "lueDkHuyen";
            this.lueDkHuyen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDkHuyen.Properties.DataSource = this.bindingSourceQuanHuyen;
            this.lueDkHuyen.Properties.DisplayMember = "TenQuanHuyen";
            this.lueDkHuyen.Properties.ValueMember = "MaQuanHuyen";
            this.lueDkHuyen.Properties.View = this.gridView3;
            this.lueDkHuyen.Size = new System.Drawing.Size(132, 20);
            this.lueDkHuyen.TabIndex = 37;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Tỉnh Thành";
            this.gridColumn1.FieldName = "TinhThanh";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Mã Quận Huyện";
            this.gridColumn2.FieldName = "MaQuanHuyen";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Tên Quận Huyện";
            this.gridColumn3.FieldName = "TenQuanHuyen";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            // 
            // lueDkTinh
            // 
            this.lueDkTinh.Location = new System.Drawing.Point(256, 186);
            this.lueDkTinh.Name = "lueDkTinh";
            this.lueDkTinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDkTinh.Properties.DataSource = this.bindingSourceTinhThanh;
            this.lueDkTinh.Properties.DisplayMember = "TenTinh";
            this.lueDkTinh.Properties.ValueMember = "MaTinh";
            this.lueDkTinh.Properties.View = this.gridView2;
            this.lueDkTinh.Size = new System.Drawing.Size(132, 20);
            this.lueDkTinh.TabIndex = 36;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Mã Tỉnh";
            this.gridColumn4.FieldName = "MaTinh";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Tên Tỉnh";
            this.gridColumn5.FieldName = "TenTinh";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Ký Hiệu";
            this.gridColumn6.FieldName = "VietTat";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            // 
            // lueQueHuyen
            // 
            this.lueQueHuyen.Location = new System.Drawing.Point(533, 132);
            this.lueQueHuyen.Name = "lueQueHuyen";
            this.lueQueHuyen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueQueHuyen.Properties.DataSource = this.bindingSourceQuanHuyen;
            this.lueQueHuyen.Properties.DisplayMember = "TenQuanHuyen";
            this.lueQueHuyen.Properties.ValueMember = "MaQuanHuyen";
            this.lueQueHuyen.Properties.View = this.gridView1;
            this.lueQueHuyen.Size = new System.Drawing.Size(132, 20);
            this.lueQueHuyen.TabIndex = 35;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTinhThanh,
            this.colMaQuanHuyen,
            this.colTenQuanHuyen});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colTinhThanh
            // 
            this.colTinhThanh.Caption = "Tỉnh Thành";
            this.colTinhThanh.FieldName = "TinhThanh";
            this.colTinhThanh.Name = "colTinhThanh";
            this.colTinhThanh.Visible = true;
            this.colTinhThanh.VisibleIndex = 2;
            // 
            // colMaQuanHuyen
            // 
            this.colMaQuanHuyen.Caption = "Mã Quận Huyện";
            this.colMaQuanHuyen.FieldName = "MaQuanHuyen";
            this.colMaQuanHuyen.Name = "colMaQuanHuyen";
            this.colMaQuanHuyen.Visible = true;
            this.colMaQuanHuyen.VisibleIndex = 0;
            // 
            // colTenQuanHuyen
            // 
            this.colTenQuanHuyen.Caption = "Tên Quận Huyện";
            this.colTenQuanHuyen.FieldName = "TenQuanHuyen";
            this.colTenQuanHuyen.Name = "colTenQuanHuyen";
            this.colTenQuanHuyen.Visible = true;
            this.colTenQuanHuyen.VisibleIndex = 1;
            // 
            // lueQueTinh
            // 
            this.lueQueTinh.Location = new System.Drawing.Point(256, 132);
            this.lueQueTinh.Name = "lueQueTinh";
            this.lueQueTinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueQueTinh.Properties.DataSource = this.bindingSourceTinhThanh;
            this.lueQueTinh.Properties.DisplayMember = "TenTinh";
            this.lueQueTinh.Properties.ValueMember = "MaTinh";
            this.lueQueTinh.Properties.View = this.searchLookUpEdit1View;
            this.lueQueTinh.Size = new System.Drawing.Size(132, 20);
            this.lueQueTinh.TabIndex = 34;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMaTinh,
            this.colTenTinh,
            this.colVietTat});
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colMaTinh
            // 
            this.colMaTinh.Caption = "Mã Tỉnh";
            this.colMaTinh.FieldName = "MaTinh";
            this.colMaTinh.Name = "colMaTinh";
            this.colMaTinh.Visible = true;
            this.colMaTinh.VisibleIndex = 0;
            // 
            // colTenTinh
            // 
            this.colTenTinh.Caption = "Tên Tỉnh";
            this.colTenTinh.FieldName = "TenTinh";
            this.colTenTinh.Name = "colTenTinh";
            this.colTenTinh.Visible = true;
            this.colTenTinh.VisibleIndex = 1;
            // 
            // colVietTat
            // 
            this.colVietTat.Caption = "Ký Hiệu";
            this.colVietTat.FieldName = "VietTat";
            this.colVietTat.Name = "colVietTat";
            this.colVietTat.Visible = true;
            this.colVietTat.VisibleIndex = 2;
            // 
            // btnLuu
            // 
            this.btnLuu.BackColor = System.Drawing.Color.Lime;
            this.btnLuu.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLuu.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnLuu.Location = new System.Drawing.Point(428, 235);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(105, 34);
            this.btnLuu.TabIndex = 32;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.UseVisualStyleBackColor = false;
            this.btnLuu.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // pbxAnh
            // 
            this.pbxAnh.Location = new System.Drawing.Point(774, 46);
            this.pbxAnh.Name = "pbxAnh";
            this.pbxAnh.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pbxAnh.Size = new System.Drawing.Size(138, 180);
            this.pbxAnh.TabIndex = 33;
            // 
            // dgThongTin
            // 
            this.dgThongTin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgThongTin.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colHoTen,
            this.colGioiTinh,
            this.colNgaySinh,
            this.colQue,
            this.colDkHKTT,
            this.colAnh});
            this.dgThongTin.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgThongTin.Location = new System.Drawing.Point(0, 280);
            this.dgThongTin.Name = "dgThongTin";
            this.dgThongTin.RowHeadersWidth = 42;
            this.dgThongTin.Size = new System.Drawing.Size(954, 274);
            this.dgThongTin.TabIndex = 35;
            // 
            // colHoTen
            // 
            this.colHoTen.FillWeight = 27.41117F;
            this.colHoTen.HeaderText = "Họ Tên";
            this.colHoTen.MinimumWidth = 52;
            this.colHoTen.Name = "colHoTen";
            this.colHoTen.Width = 161;
            // 
            // colGioiTinh
            // 
            this.colGioiTinh.FillWeight = 27.41117F;
            this.colGioiTinh.HeaderText = "Giới Tính";
            this.colGioiTinh.Name = "colGioiTinh";
            this.colGioiTinh.Width = 162;
            // 
            // colNgaySinh
            // 
            this.colNgaySinh.FillWeight = 27.41117F;
            this.colNgaySinh.HeaderText = "Ngày Sinh";
            this.colNgaySinh.Name = "colNgaySinh";
            this.colNgaySinh.Width = 163;
            // 
            // colQue
            // 
            this.colQue.FillWeight = 27.41117F;
            this.colQue.HeaderText = "Quê Quán";
            this.colQue.Name = "colQue";
            this.colQue.Width = 162;
            // 
            // colDkHKTT
            // 
            this.colDkHKTT.FillWeight = 27.41117F;
            this.colDkHKTT.HeaderText = "Nơi ĐK HKTT";
            this.colDkHKTT.Name = "colDkHKTT";
            this.colDkHKTT.Width = 162;
            // 
            // colAnh
            // 
            this.colAnh.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colAnh.FillWeight = 462.9442F;
            this.colAnh.HeaderText = "Ảnh";
            this.colAnh.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.colAnh.MinimumWidth = 100;
            this.colAnh.Name = "colAnh";
            this.colAnh.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // frmDangKy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(954, 527);
            this.Controls.Add(this.dgThongTin);
            this.Controls.Add(this.dtpBirthDay);
            this.Controls.Add(this.cbNu);
            this.Controls.Add(this.cbNam);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Name = "frmDangKy";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceTinhThanh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceQuanHuyen)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueDkHuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDkTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueQueHuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueQueTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxAnh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgThongTin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.CheckBox cbNam;
        private System.Windows.Forms.CheckBox cbNu;
        private System.Windows.Forms.DateTimePicker dtpBirthDay;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.BindingSource bindingSourceTinhThanh;
        private System.Windows.Forms.BindingSource bindingSourceQuanHuyen;
        private System.Windows.Forms.DataGridView dgThongTin;
        private System.Windows.Forms.Button btnLuu;
        private System.Windows.Forms.DataGridViewTextBoxColumn colHoTen;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGioiTinh;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNgaySinh;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQue;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDkHKTT;
        private System.Windows.Forms.DataGridViewImageColumn colAnh;
        private DevExpress.XtraEditors.PictureEdit pbxAnh;
        private DevExpress.XtraEditors.SearchLookUpEdit lueQueTinh;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colMaTinh;
        private DevExpress.XtraGrid.Columns.GridColumn colTenTinh;
        private DevExpress.XtraGrid.Columns.GridColumn colVietTat;
        private DevExpress.XtraEditors.SearchLookUpEdit lueDkTinh;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.SearchLookUpEdit lueQueHuyen;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SearchLookUpEdit lueDkHuyen;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colTinhThanh;
        private DevExpress.XtraGrid.Columns.GridColumn colMaQuanHuyen;
        private DevExpress.XtraGrid.Columns.GridColumn colTenQuanHuyen;

    }
}

