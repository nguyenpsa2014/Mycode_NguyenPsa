﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Caculator1
{
    public partial class Form1 : Form
    {
        private bool kt, flag=false;
        private string pt, value, value1;
        private double kq = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void NhanNut(string btn)
        {
            if (flag == true)
            {
                value = "";
                value1 = "";
                tbx.Text = "";
                kq = 0;
                flag = false;
            }
            if (kt)
            {
                tbx.Text += btn;
                value1 += btn;
            }
            else
            {
                tbx.Text += btn;
                kt = false;
            }
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            NhanNut("0");
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            NhanNut("1");
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            NhanNut("2");
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            NhanNut("3");
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            NhanNut("4");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            NhanNut("5");
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            NhanNut("6");
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            NhanNut("7");
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            NhanNut("8");
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            NhanNut("9");
        }

        private void btndel_Click(object sender, EventArgs e)
        {
            tbx.Text = tbx.Text.Substring(0, tbx.Text.Length - 1);
        }

        private void btnmod_Click(object sender, EventArgs e)
        {
            tbx.Clear();
        }

        private void btncham_Click(object sender, EventArgs e)
        {
            if (kt)
            {
                tbx.Text += ".";
                value1 += ".";
            }
            else
            {
                tbx.Text += ".";
                kt = false;
            }
        }

        private void btnchia_Click(object sender, EventArgs e)
        {
            value = tbx.Text;
            tbx.Text+= "/";
            kt = true;
            pt = "/";
        }

        private void btnnhan_Click(object sender, EventArgs e)
        {
            value = tbx.Text;
            tbx.Text += "*";
            kt = true;
            pt = "*";
        }

        private void btntru_Click(object sender, EventArgs e)
        {
            value = tbx.Text;
            tbx.Text += "-";
            kt = true;
            pt = "-";
        }

        private void btncong_Click(object sender, EventArgs e)
        {
            value = tbx.Text;
            tbx.Text += "+";
            kt = true;
            pt = "+";
        }

        private void btnkq_Click(object sender, EventArgs e)
        {
            double a, b;
            flag = true;
            kt = false;
            a = Convert.ToDouble(value);
            b = Convert.ToDouble(value1);
            switch (pt)
            {
                case "+":
                    kq = a + b;
                    break;
                case "-":
                    kq = a - b;
                    break;
                case "*":
                    kq = a * b;
                    break;
                case "/":
                    kq = a / b;
                    break;
            }
            //tbxkq.Text = Convert.ToString(-kq);
            if (kq >= 0)
                tbxkq.Text = Convert.ToString(kq);
            else tbxkq.Text = Convert.ToString(-kq) + "-";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NhanNut("-");
        }

    }
}
