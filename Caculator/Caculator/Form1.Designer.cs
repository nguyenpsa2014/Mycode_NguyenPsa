﻿namespace Caculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbx = new System.Windows.Forms.TextBox();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btnchia = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btncham = new System.Windows.Forms.Button();
            this.btnnhan = new System.Windows.Forms.Button();
            this.btnmod = new System.Windows.Forms.Button();
            this.btncong = new System.Windows.Forms.Button();
            this.btntru = new System.Windows.Forms.Button();
            this.btnkq = new System.Windows.Forms.Button();
            this.btndel = new System.Windows.Forms.Button();
            this.btnoff = new System.Windows.Forms.Button();
            this.tbxkq = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbx
            // 
            this.tbx.BackColor = System.Drawing.SystemColors.Window;
            this.tbx.Font = new System.Drawing.Font("Segoe UI Symbol", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbx.ForeColor = System.Drawing.SystemColors.InfoText;
            this.tbx.Location = new System.Drawing.Point(22, 12);
            this.tbx.Multiline = true;
            this.tbx.Name = "tbx";
            this.tbx.ReadOnly = true;
            this.tbx.Size = new System.Drawing.Size(362, 85);
            this.tbx.TabIndex = 0;
            // 
            // btn7
            // 
            this.btn7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.Location = new System.Drawing.Point(22, 141);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(58, 40);
            this.btn7.TabIndex = 1;
            this.btn7.Text = "7";
            this.btn7.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btn7.UseVisualStyleBackColor = false;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Location = new System.Drawing.Point(22, 263);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(58, 40);
            this.btn1.TabIndex = 2;
            this.btn1.Text = "1";
            this.btn1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn6
            // 
            this.btn6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.Location = new System.Drawing.Point(172, 203);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(58, 40);
            this.btn6.TabIndex = 3;
            this.btn6.Text = "6";
            this.btn6.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btn6.UseVisualStyleBackColor = false;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn5
            // 
            this.btn5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.Location = new System.Drawing.Point(97, 203);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(58, 40);
            this.btn5.TabIndex = 4;
            this.btn5.Text = "5";
            this.btn5.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btn5.UseVisualStyleBackColor = false;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn4
            // 
            this.btn4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.Location = new System.Drawing.Point(22, 203);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(58, 40);
            this.btn4.TabIndex = 5;
            this.btn4.Text = "4";
            this.btn4.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btn4.UseVisualStyleBackColor = false;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn9
            // 
            this.btn9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.Location = new System.Drawing.Point(172, 141);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(58, 40);
            this.btn9.TabIndex = 6;
            this.btn9.Text = "9";
            this.btn9.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btn9.UseVisualStyleBackColor = false;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btn8
            // 
            this.btn8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.Location = new System.Drawing.Point(97, 141);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(58, 40);
            this.btn8.TabIndex = 7;
            this.btn8.Text = "8";
            this.btn8.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btn8.UseVisualStyleBackColor = false;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btnchia
            // 
            this.btnchia.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnchia.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnchia.Location = new System.Drawing.Point(326, 203);
            this.btnchia.Name = "btnchia";
            this.btnchia.Size = new System.Drawing.Size(58, 40);
            this.btnchia.TabIndex = 8;
            this.btnchia.Text = "/";
            this.btnchia.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnchia.UseVisualStyleBackColor = false;
            this.btnchia.Click += new System.EventHandler(this.btnchia_Click);
            // 
            // btn0
            // 
            this.btn0.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn0.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.Location = new System.Drawing.Point(22, 327);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(58, 40);
            this.btn0.TabIndex = 9;
            this.btn0.Text = "0";
            this.btn0.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btn0.UseVisualStyleBackColor = false;
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn3
            // 
            this.btn3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.Location = new System.Drawing.Point(172, 263);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(58, 40);
            this.btn3.TabIndex = 10;
            this.btn3.Text = "3";
            this.btn3.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btn3.UseVisualStyleBackColor = false;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn2
            // 
            this.btn2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.Location = new System.Drawing.Point(97, 263);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(58, 40);
            this.btn2.TabIndex = 11;
            this.btn2.Text = "2";
            this.btn2.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btn2.UseVisualStyleBackColor = false;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btncham
            // 
            this.btncham.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btncham.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncham.Location = new System.Drawing.Point(97, 327);
            this.btncham.Name = "btncham";
            this.btncham.Size = new System.Drawing.Size(58, 40);
            this.btncham.TabIndex = 12;
            this.btncham.Text = ".";
            this.btncham.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btncham.UseVisualStyleBackColor = false;
            this.btncham.Click += new System.EventHandler(this.btncham_Click);
            // 
            // btnnhan
            // 
            this.btnnhan.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnnhan.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnhan.Location = new System.Drawing.Point(250, 203);
            this.btnnhan.Name = "btnnhan";
            this.btnnhan.Size = new System.Drawing.Size(58, 40);
            this.btnnhan.TabIndex = 13;
            this.btnnhan.Text = "*";
            this.btnnhan.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnnhan.UseVisualStyleBackColor = false;
            this.btnnhan.Click += new System.EventHandler(this.btnnhan_Click);
            // 
            // btnmod
            // 
            this.btnmod.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnmod.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmod.Location = new System.Drawing.Point(326, 141);
            this.btnmod.Name = "btnmod";
            this.btnmod.Size = new System.Drawing.Size(58, 40);
            this.btnmod.TabIndex = 14;
            this.btnmod.Text = "AC";
            this.btnmod.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnmod.UseVisualStyleBackColor = false;
            this.btnmod.Click += new System.EventHandler(this.btnmod_Click);
            // 
            // btncong
            // 
            this.btncong.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btncong.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncong.Location = new System.Drawing.Point(326, 263);
            this.btncong.Name = "btncong";
            this.btncong.Size = new System.Drawing.Size(58, 40);
            this.btncong.TabIndex = 15;
            this.btncong.Text = "+";
            this.btncong.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btncong.UseVisualStyleBackColor = false;
            this.btncong.Click += new System.EventHandler(this.btncong_Click);
            // 
            // btntru
            // 
            this.btntru.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btntru.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btntru.Location = new System.Drawing.Point(250, 263);
            this.btntru.Name = "btntru";
            this.btntru.Size = new System.Drawing.Size(58, 40);
            this.btntru.TabIndex = 16;
            this.btntru.Text = "-";
            this.btntru.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btntru.UseVisualStyleBackColor = false;
            this.btntru.Click += new System.EventHandler(this.btntru_Click);
            // 
            // btnkq
            // 
            this.btnkq.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnkq.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnkq.Location = new System.Drawing.Point(250, 327);
            this.btnkq.Name = "btnkq";
            this.btnkq.Size = new System.Drawing.Size(134, 40);
            this.btnkq.TabIndex = 17;
            this.btnkq.Text = "=";
            this.btnkq.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnkq.UseVisualStyleBackColor = false;
            this.btnkq.Click += new System.EventHandler(this.btnkq_Click);
            // 
            // btndel
            // 
            this.btndel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btndel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndel.Location = new System.Drawing.Point(172, 328);
            this.btndel.Name = "btndel";
            this.btndel.Size = new System.Drawing.Size(58, 40);
            this.btndel.TabIndex = 18;
            this.btndel.Text = "DEL";
            this.btndel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btndel.UseVisualStyleBackColor = false;
            this.btndel.Click += new System.EventHandler(this.btndel_Click);
            // 
            // btnoff
            // 
            this.btnoff.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnoff.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnoff.Location = new System.Drawing.Point(250, 141);
            this.btnoff.Name = "btnoff";
            this.btnoff.Size = new System.Drawing.Size(58, 40);
            this.btnoff.TabIndex = 19;
            this.btnoff.Text = "OFF";
            this.btnoff.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnoff.UseVisualStyleBackColor = false;
            this.btnoff.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbxkq
            // 
            this.tbxkq.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxkq.Location = new System.Drawing.Point(22, 51);
            this.tbxkq.Multiline = true;
            this.tbxkq.Name = "tbxkq";
            this.tbxkq.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbxkq.Size = new System.Drawing.Size(362, 46);
            this.tbxkq.TabIndex = 20;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 399);
            this.Controls.Add(this.tbxkq);
            this.Controls.Add(this.btnoff);
            this.Controls.Add(this.btndel);
            this.Controls.Add(this.btnkq);
            this.Controls.Add(this.btntru);
            this.Controls.Add(this.btncong);
            this.Controls.Add(this.btnmod);
            this.Controls.Add(this.btnnhan);
            this.Controls.Add(this.btncham);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btnchia);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.tbx);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbx;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btnchia;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btncham;
        private System.Windows.Forms.Button btnnhan;
        private System.Windows.Forms.Button btnmod;
        private System.Windows.Forms.Button btncong;
        private System.Windows.Forms.Button btntru;
        private System.Windows.Forms.Button btnkq;
        private System.Windows.Forms.Button btndel;
        private System.Windows.Forms.Button btnoff;
        private System.Windows.Forms.TextBox tbxkq;
    }
}

