﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Caculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public int gt = 0;
        public string value = null, value1 = null;
        private bool key = false;
        private bool flat = false;

        public void NhanNut(string val)
        {
            if (flat == true)
            {
                tbx.Text = "";
                flat = false;
            }
            if (key == false)
            {
                tbx.Text += val;
                value = tbx.Text;
            }
            else
            {
                tbx.Text += val;
                value1 += val;
            }

        }
        public void PhepTinh(string pt)
        {
            string tmp = "";
            tmp = pt;
            //tbx.Text="";
            key = true;
            switch (tmp)
            {

                case "+":
                    gt = 1;
                    tbx.Text += "+";
                    break;
                case "-":
                    gt = 2;
                    tbx.Text += "-";
                    break;
                case "*":
                    gt = 3;
                    tbx.Text += "*";
                    break;
                case "/":
                    gt = 4;
                    tbx.Text += "/";
                    break;
                default:
                    break;
            }
        }
        private void btn1_Click(object sender, EventArgs e)
        {
            NhanNut("1");
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            NhanNut("2");
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            NhanNut("3");
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            NhanNut("4");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            NhanNut("5");
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            NhanNut("6");
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            NhanNut("7");
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            NhanNut("8");
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            NhanNut("9");
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            NhanNut("0");
        }

        private void btncham_Click(object sender, EventArgs e)
        {
            NhanNut(".");
        }

        private void btndel_Click(object sender, EventArgs e)
        {
            tbx.Text = tbx.Text.Substring(0, tbx.Text.Length - 1);
        }

        private void btnchia_Click(object sender, EventArgs e)
        {
            PhepTinh("/");
        }

        private void btnnhan_Click(object sender, EventArgs e)
        {
            PhepTinh("*");
        }

        private void btntru_Click(object sender, EventArgs e)
        {
            PhepTinh("-");
        }

        private void btncong_Click(object sender, EventArgs e)
        {
            PhepTinh("+");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnkq_Click(object sender, EventArgs e)
        {
            tbxkq.Text = "";
            if (key == false)
                tbx.Text = value;
            else
            {
                double a, b;
                flat = true;
                
                a = Convert.ToDouble(value);
                b = Convert.ToDouble(value1);
                switch (gt)
                {
                    case 1:
                        tbxkq.Text = Convert.ToString(a + b);
                        break;
                    case 2:
                        tbxkq.Text = Convert.ToString(a - b);
                        break;
                    case 3:
                        tbxkq.Text = Convert.ToString(a * b);
                        break;
                    case 4:
                        tbxkq.Text = Convert.ToString(a / b);
                        break;
                }
            }
        }

        private void btnmod_Click(object sender, EventArgs e)
        {
            tbx.Text = null;
            gt = 0;
        }
    }
}
