﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BaiHocSQL.Database;
using System.Data.Entity;

namespace BaiHocSQL.View
{
    public partial class frmSinhVien : DevExpress.XtraEditors.XtraForm
    {
        dbSinhVien db = new dbSinhVien();
        public frmSinhVien()
        {
            InitializeComponent();
        }

        private void frmSinhVien_Load(object sender, EventArgs e)
        {
            db.LopHocs.Load();
            lopHocBindingSource.DataSource = db.LopHocs.Local.ToBindingList();
            db.SinhViens.Load();
            sinhVienBindingSource.DataSource = db.SinhViens.Local.ToBindingList();
            

        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Bạn có muốn xóa không", "Warning!!!",
                MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
            {
                
               sinhVienBindingSource.RemoveCurrent();

            }
        }

        private void sinhVienBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            bindingNavigatorPositionItem.Focus();
            sinhVienBindingSource.EndEdit();
            db.SaveChanges();
            XtraMessageBox.Show("Đã Lưu");
        }
    }
}