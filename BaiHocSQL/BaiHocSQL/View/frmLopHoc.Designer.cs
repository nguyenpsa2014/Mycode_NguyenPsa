﻿namespace BaiHocSQL.View
{
    partial class frmLopHoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lopHoc1Label;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLopHoc));
            this.lopHocBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lopHocBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.lopHocBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.lopHocGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLopHoc1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDLop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDLopEdit = new DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit();
            this.repositoryItemSearchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLopHoc1Edit = new DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lopHoc1TextEdit = new DevExpress.XtraEditors.TextEdit();
            lopHoc1Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lopHocBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lopHocBindingNavigator)).BeginInit();
            this.lopHocBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lopHocGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colIDLopEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSearchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colLopHoc1Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lopHoc1TextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lopHoc1Label
            // 
            lopHoc1Label.AutoSize = true;
            lopHoc1Label.Location = new System.Drawing.Point(55, 39);
            lopHoc1Label.Name = "lopHoc1Label";
            lopHoc1Label.Size = new System.Drawing.Size(45, 13);
            lopHoc1Label.TabIndex = 2;
            lopHoc1Label.Text = "Lớp Học";
            // 
            // lopHocBindingSource
            // 
            this.lopHocBindingSource.DataSource = typeof(BaiHocSQL.Database.LopHoc);
            // 
            // lopHocBindingNavigator
            // 
            this.lopHocBindingNavigator.AddNewItem = null;
            this.lopHocBindingNavigator.BindingSource = this.lopHocBindingSource;
            this.lopHocBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.lopHocBindingNavigator.DeleteItem = null;
            this.lopHocBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.lopHocBindingNavigatorSaveItem});
            this.lopHocBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.lopHocBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.lopHocBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.lopHocBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.lopHocBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.lopHocBindingNavigator.Name = "lopHocBindingNavigator";
            this.lopHocBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.lopHocBindingNavigator.Size = new System.Drawing.Size(717, 25);
            this.lopHocBindingNavigator.TabIndex = 0;
            this.lopHocBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(105, 22);
            this.bindingNavigatorAddNewItem.Text = "Thêm Mơi (F8)";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(70, 22);
            this.bindingNavigatorDeleteItem.Text = "Xóa (F9)";
            this.bindingNavigatorDeleteItem.Click += new System.EventHandler(this.bindingNavigatorDeleteItem_Click);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // lopHocBindingNavigatorSaveItem
            // 
            this.lopHocBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("lopHocBindingNavigatorSaveItem.Image")));
            this.lopHocBindingNavigatorSaveItem.Name = "lopHocBindingNavigatorSaveItem";
            this.lopHocBindingNavigatorSaveItem.Size = new System.Drawing.Size(76, 22);
            this.lopHocBindingNavigatorSaveItem.Text = "Lưu (F10)";
            this.lopHocBindingNavigatorSaveItem.Click += new System.EventHandler(this.lopHocBindingNavigatorSaveItem_Click);
            // 
            // lopHocGridControl
            // 
            this.lopHocGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.lopHocGridControl.DataSource = this.lopHocBindingSource;
            this.lopHocGridControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lopHocGridControl.Location = new System.Drawing.Point(0, 62);
            this.lopHocGridControl.MainView = this.gridView1;
            this.lopHocGridControl.Name = "lopHocGridControl";
            this.lopHocGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.colIDLopEdit,
            this.colLopHoc1Edit});
            this.lopHocGridControl.Size = new System.Drawing.Size(717, 396);
            this.lopHocGridControl.TabIndex = 1;
            this.lopHocGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.lopHocGridControl.Click += new System.EventHandler(this.lopHocGridControl_Click);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLopHoc1,
            this.colIDLop});
            this.gridView1.GridControl = this.lopHocGridControl;
            this.gridView1.Name = "gridView1";
            // 
            // colLopHoc1
            // 
            this.colLopHoc1.Caption = "Lớp Học";
            this.colLopHoc1.FieldName = "LopHoc1";
            this.colLopHoc1.Name = "colLopHoc1";
            this.colLopHoc1.OptionsColumn.AllowEdit = false;
            this.colLopHoc1.Visible = true;
            this.colLopHoc1.VisibleIndex = 1;
            // 
            // colIDLop
            // 
            this.colIDLop.Caption = "Mã Lớp Học";
            this.colIDLop.FieldName = "IDLop";
            this.colIDLop.Name = "colIDLop";
            this.colIDLop.OptionsColumn.AllowEdit = false;
            this.colIDLop.Visible = true;
            this.colIDLop.VisibleIndex = 0;
            // 
            // colIDLopEdit
            // 
            this.colIDLopEdit.AutoHeight = false;
            this.colIDLopEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colIDLopEdit.Name = "colIDLopEdit";
            this.colIDLopEdit.View = this.repositoryItemSearchLookUpEdit1View;
            // 
            // repositoryItemSearchLookUpEdit1View
            // 
            this.repositoryItemSearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemSearchLookUpEdit1View.Name = "repositoryItemSearchLookUpEdit1View";
            this.repositoryItemSearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemSearchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colLopHoc1Edit
            // 
            this.colLopHoc1Edit.AutoHeight = false;
            this.colLopHoc1Edit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colLopHoc1Edit.Name = "colLopHoc1Edit";
            this.colLopHoc1Edit.View = this.gridView2;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // lopHoc1TextEdit
            // 
            this.lopHoc1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.lopHocBindingSource, "LopHoc1", true));
            this.lopHoc1TextEdit.EditValue = "";
            this.lopHoc1TextEdit.Location = new System.Drawing.Point(116, 36);
            this.lopHoc1TextEdit.Name = "lopHoc1TextEdit";
            this.lopHoc1TextEdit.Size = new System.Drawing.Size(136, 20);
            this.lopHoc1TextEdit.TabIndex = 3;
            // 
            // frmLopHoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 458);
            this.Controls.Add(lopHoc1Label);
            this.Controls.Add(this.lopHocGridControl);
            this.Controls.Add(this.lopHocBindingNavigator);
            this.Controls.Add(this.lopHoc1TextEdit);
            this.Name = "frmLopHoc";
            this.Text = "Danh Sách Lớp Học";
            this.Load += new System.EventHandler(this.frmLopHoc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lopHocBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lopHocBindingNavigator)).EndInit();
            this.lopHocBindingNavigator.ResumeLayout(false);
            this.lopHocBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lopHocGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colIDLopEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSearchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colLopHoc1Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lopHoc1TextEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource lopHocBindingSource;
        private System.Windows.Forms.BindingNavigator lopHocBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton lopHocBindingNavigatorSaveItem;
        private DevExpress.XtraGrid.GridControl lopHocGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colLopHoc1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit colLopHoc1Edit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colIDLop;
        private DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit colIDLopEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemSearchLookUpEdit1View;
        private DevExpress.XtraEditors.TextEdit lopHoc1TextEdit;
    }
}