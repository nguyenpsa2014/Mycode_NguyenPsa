﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BaiHocSQL.Database;
using System.Data.Entity;

namespace BaiHocSQL.View
{
    public partial class frmLopHoc : DevExpress.XtraEditors.XtraForm
    {           
        dbSinhVien db = new dbSinhVien();
        public frmLopHoc()
        {
            InitializeComponent();
        }

        private void frmLopHoc_Load(object sender, EventArgs e)
        {
            db.LopHocs.Load();
            lopHocBindingSource.DataSource = db.LopHocs.Local.ToBindingList(); 
        }

        private void lopHocBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            bindingNavigatorPositionItem.Focus();
            lopHocBindingSource.EndEdit();
            db.SaveChanges();
            XtraMessageBox.Show("Đã Lưu");
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Bạn có muốn xóa không", "Warning!!!",
                MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
            {
                lopHocBindingSource.RemoveCurrent();
                
            }
        }

        private void lopHocGridControl_Click(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

            lopHocBindingSource.EndEdit();
            
        }
    }
}