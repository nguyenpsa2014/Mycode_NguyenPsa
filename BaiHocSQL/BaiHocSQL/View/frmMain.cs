﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraSplashScreen;
using System.Collections;
using System.Threading;
using System.Globalization;

namespace BaiHocSQL.View
{
    public partial class frmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {

        bool lophoc = false, sinhvien = false;
        public frmMain()
        {
            InitializeComponent();

        }

        private void btnLopHoc_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmLopHoc frmLopHoc = new frmLopHoc();
            if (!lophoc)
            {
                
                //gán frmMain là from Cha
                frmLopHoc.MdiParent = this;
                frmLopHoc.Show();
            }
            lophoc = true;


        }

        private void btnSinhVien_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!sinhvien)
            {
                frmSinhVien frmSinhVien = new frmSinhVien();
                //gán frmMain là from Cha
                frmSinhVien.MdiParent = this;
                frmSinhVien.Show();
            }
            sinhvien = true;

        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }
    }
}