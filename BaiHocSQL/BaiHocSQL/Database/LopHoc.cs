namespace BaiHocSQL.Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LopHoc")]
    public partial class LopHoc
    {
        public LopHoc()
        {
            SinhViens = new HashSet<SinhVien>();
        }

        [Key]
        public int IDLop { get; set; }

        [Column("LopHoc")]
        [StringLength(100)]
        public string LopHoc1 { get; set; }

        public virtual ICollection<SinhVien> SinhViens { get; set; }
    }
}
